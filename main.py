import json
import math
from pprint import pprint
from termcolor import colored

with open('/home/nicolas/Documents/Exercices/exo trinker/exo_algo_trinker_python/people.json', 'r') as p:
    people = json.loads(p.read())

print(colored("""
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   
""", 'yellow'))
print(colored('Modele des données :', 'yellow'))
pprint(people[0])

# debut de l'exo
print(colored(''.join(['_' for _ in range(80)]), 'green', 'on_green'))

print(colored("Nombre d'hommes : ", 'yellow'))
# pour chaque personne du tableau, si son genre == 'Male' je le met dans le tableau hommes
hommes = [p for p in people if p['gender'] == 'Male']
# len() revoie la taille (nombre d'élément) d'un tableau
pprint(len(hommes))

################################################################################

# je peux aussi l'écrire avec une boucle classique
hommes2 = []                        # un tableau vide
for person in people:               # pour chaque persone du tableau
    if person["gender"] == "Male":  # si c'est un homme (2-266-02250-4)
        hommes2.append(person)      # je l'ajoute au tableau
print(len(hommes2))

################################################################################

# dans la même idée, plutot que de mettre tous les hommes dans un tableau
# puis afficher la longueur du tableau, je peux juste les compter dans une variable
nb_hommes = 0                       # je commence à 0
for person in people:               # pour chaque persone du tableau
    if person["gender"] == "Male":  # si c'est un homme
        nb_hommes = nb_hommes + 1   # j'ajoute 1 à mon compteur
print(nb_hommes)

################################################################################
print(colored("Nombre de femmes : ", 'yellow'))
# je peux compter les femmes ou calculer : nombre d'élement dans people - nombre d'homme
nb_femmes = 0   
for person in people:               
    if person["gender"] == "Female":  
        nb_femmes = nb_femmes + 1   
print(nb_femmes)


################################################################################

print(colored("Nombre de personnes qui cherchent un homme :", 'yellow'))
pc_hom = 0
for person in people:
    if person["looking_for"]=="M":
        pc_hom = pc_hom + 1
print(pc_hom)

################################################################################

print(colored("Nombre de personnes qui cherchent une femme :", 'yellow'))
pc_fem = 0
for person in people:
    if person["looking_for"]=="F":
        pc_fem = pc_fem + 1
print(pc_fem)

################################################################################

print(colored("Nombre de personnes qui gagnent plus de 2000$ :", 'yellow'))
income=0
for person in people:
   if float(person["income"][1:])> 2000:
        income = income + 1
print(income)


################################################################################

print(colored("Nombre de personnes qui aiment les Drama :", 'yellow'))
# là il va falloir regarder si le chaine de charactères "Drama" se trouve dans "pref_movie"
p_drama = 0
for person in people:
    if "Drama" in person["pref_movie"]:
        p_drama = p_drama + 1
print(p_drama)

################################################################################

print(colored("Nombre de femmes qui aiment la science-fiction :", 'yellow'))
# si j'ai déjà un tableau avec toutes les femmes, je peux chercher directement dedans ;)
p_scifi = 0
for person in people:
    if "Sci-Fi" in person["pref_movie"] and person["gender"] == "Female":
        p_scifi = p_scifi + 1
print(p_scifi)

################################################################################

print(colored('LEVEL 2' + ''.join(['_' for _ in range(73)]), 'green', 'on_green'))

################################################################################

print(colored("Nombre de personnes qui aiment les documentaires et gagnent plus de 1482$", 'yellow'))
pdoc1482 = 0
for person in people:
    if "Documentary" in person["pref_movie"] and float(person["income"][1:])> 1482:
        pdoc1482 = pdoc1482 + 1
print(pdoc1482)


################################################################################

print(colored("Liste des noms, prénoms, id et revenus des personnes qui gagnent plus de 4000$", 'yellow'))
for person in people:
    if float(person["income"][1:])> 4000:
        print(person["id"], person["first_name"],person["last_name"], person["income"])


################################################################################

print(colored("Homme le plus riche (nom et id) :", 'yellow'))
le_plus_riche = max(hommes, key=lambda homme:float(homme["income"][1:]))
print(le_plus_riche ["id"], le_plus_riche ["last_name"])

################################################################################

print(colored("Salaire moyen :", 'yellow'))
total=0
for person in people:
    total = total + float(person["income"][1:])
m=total / len(people)
print(m)

################################################################################

print(colored("Salaire médian :", 'yellow'))
tab =[] # je crée un tableau
for person in people:
    tab.append(float(person["income"][1:]))
sorted_tab = sorted(tab) # je trie le tableau
nbre_val = len(sorted_tab)  # j'obtiens le nombre de valeurs du tableau 
print(nbre_val)
mediane = sorted_tab[499] + sorted_tab [500] 
print(mediane/2)


################################################################################

print(colored("Nombre de personnes qui habitent dans l'hémisphère nord :", 'yellow'))
pers_nord = 0

for person in people:
    latitude = (person["latitude"])
    if 0 <= latitude <= 90:
        pers_nord += 1

print(pers_nord)



################################################################################

print(colored("Salaire moyen des personnes qui habitent dans l'hémisphère sud :", 'yellow'))
s_sud = 0 # calcul du nombre de personnes qui habitent dans l'hémisphère sud
for person in people:
   if (person["latitude"]) < 0:
        s_sud += float(person["income"][1:]) # total salaire hémisphère sud
sal_moyen_sud = s_sud/(1000-pers_nord)
print(sal_moyen_sud)


################################################################################

print(colored('LEVEL 3' + ''.join(['_' for _ in range(73)]), 'green', 'on_green'))

################################################################################

print(colored("Personne qui habite le plus près de Bérénice Cawt (nom et id) :", 'yellow'))
# on définit la fonction distance (distance entre 2 poins), théorème de pythagore
def distance(p1,p2):
    B=p1["latitude"]-p2["latitude"]   
    C=p1["longitude"]-p2["longitude"]
    return math.sqrt(B**2+C**2)

# on récupère les valeurs de Bérénice, accolade car c'est un classeur
berenice = {}
for person in people:
    if person["first_name"]=="Bérénice" and person["last_name"]=="Cawt":
        berenice = person

# on récupère le tableau des distances
distances = []
# on récupère une variable pour la distance minimale (valeur 1000 au hasard)
distance_min = 1000
# on déclare une variable "le plus proche"
le_plus_proche = {}
for person in people:
    if person != berenice:
        # distance entre berenice et les autres personnes
        dis=distance(berenice, person)
        # on récupère toutes les distances entre Bérénice et les autres membres
        distances.append(dis)
    if dis < distance_min:
        # alors dis = distance minimum
            distance_min = dis
        # on obtient la position de la personne la plus proche
            le_plus_proche = person
print(le_plus_proche ["id"], le_plus_proche ["last_name"])
    










################################################################################

print(colored("Personne qui habite le plus près de Ruì Brach (nom et id) :", 'yellow'))
def distance(p1,p2):
    B=p1["latitude"]-p2["latitude"]   
    C=p1["longitude"]-p2["longitude"]
    return math.sqrt(B**2+C**2)

# on récupère les valeurs de Rui Brach, accolade car c'est un classeur
rui = {}
for person in people:
    if person["first_name"]=="Ruì" and person["last_name"]=="Brach":
        rui = person

# on récupère le tableau des distances
distances = []
# on récupère une variable pour la distance minimale (valeur 1000 au hasard)
distance_min = 1000
# on déclare une variable "le plus proche"
le_plus_proche = {}
for person in people:
    if person != rui:
        # distance entre rui et les autres personnes
        dis=distance(rui,person)
        # on récupère toutes les distances entre Bérénice et les autres membres
        distances.append(dis)
    if dis < distance_min:
        # alors dis = distance minimum
            distance_min = dis
        # on obtient la position de la personne la plus proche
            le_plus_proche = person
print(le_plus_proche ["id"], le_plus_proche ["last_name"])
    

################################################################################

print(colored("les 10 personnes qui habitent les plus près de Josée Boshard (nom et id) :", 'yellow'))
def distance(p1,p2):
    B=p1["latitude"]-p2["latitude"]   
    C=p1["longitude"]-p2["longitude"]
    return math.sqrt(B**2+C**2)

# on récupère les valeurs de Josée Boshard, accolade car c'est un classeur
josee = {}
for person in people:
    if person["first_name"]=="Josée" and person["last_name"]=="Boshard":
        josee = person

# on récupère le tableau des distances
shortest_distances = []
les_10_plus_proches = {}
for person in people:
    if person != josee:
        # distance entre josee et les autres personnes
        dis=distance(josee,person)
        # on récupère toutes les distances entre Josée et les autres membres
        distances.append(dis)
        les_10_plus_proches = person
        # on trie le tableau des distances
shortest_distance = []
for i in range(1,11):
    distance1 = []
    for j in range(1,11):
        if i==j: continue
        distance1.append(distance[i])
        shortest_distance.append(min(distance1))
print(shortest_distance)
    

    


################################################################################

print(colored("Les noms et ids des 23 personnes qui travaillent chez google :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Personne la plus agée :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Personne la plus jeune :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))
print(colored("Moyenne des différences d'age :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored('LEVEL 4' + ''.join(['_' for _ in range(73)]), 'green', 'on_green'))
print(colored("Genre de film le plus populaire :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Genres de film par ordre de popularité :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Liste des genres de film et nombre de personnes qui les préfèrent :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Age moyen des hommes qui aiment les films noirs :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Age moyen des femmes qui aiment les drames et habitent sur le fuseau horaire, de Paris : ", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("""Homme qui cherche un homme et habite le plus proche d'un homme qui a au moins une
préférence de film en commun (afficher les deux et la distance entre les deux):""", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Liste des couples femmes / hommes qui ont les même préférences de films :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored('MATCH' + ''.join(['_' for _ in range(73)]), 'green', 'on_green'))
"""
    On match les gens avec ce qu'ils cherchent (homme ou femme).
    On prend en priorité ceux qui ont le plus de gouts en commun.
    Puis ceux qui sont les plus proches.
    Les gens qui travaillent chez google ne peuvent qu'être en couple entre eux.
    Quelqu'un qui n'aime pas les Drama ne peux pas être en couple avec quelqu'un qui les aime.
    Quelqu'un qui aime les films d'aventure doit forcement être en couple avec quelqu'un qui aime aussi 
    les films d'aventure.
    La différences d'age dans un couple doit être inférieure à 25% (de l'age du plus agé des deux)
    ߷    ߷    ߷    Créer le plus de couples possibles.                  ߷    ߷    ߷    
    ߷    ߷    ߷    Mesurez le temps de calcul de votre fonction         ߷    ߷    ߷    
    ߷    ߷    ߷    Essayez de réduire le temps de calcul au maximum     ߷    ߷    ߷    

"""
print(colored("liste de couples à matcher (nom et id pour chaque membre du couple) :", 'yellow'))
print(colored('Exemple :', 'green'))
print(colored('1 Alice A.\t2 Bob B.'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))
